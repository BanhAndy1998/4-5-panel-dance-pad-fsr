#include <Keyboard.h>
#include <CircularBuffer.h>
#include <VariableTimedAction.h>
#include <SoftwareSerial.h>

#define fsrpinUL A0
#define fsrpinUR A1
#define fsrpinM A2
#define fsrpinDL A3
#define fsrpinDR A4
#define selector 

//actSens = Activation threshold. Does not deactivate upon dropping lower than actSens but does upon going lower than thresSens
//thresSens = Deactivation threshold. Does not activate upon going higher than thresSens but does up going higher than actSens
//activated = Sensor is currently sending input.
//lastInput = The last read input from the timedAction class
//timer = Keeps the program from adjusting sensitivity more than once every 500ms
//automode = Enables the Arduino to automatically adjust panel sensitivity if true
//debugmode = Prints information about each panel to the arduino's Serial output if true
bool activatedUL;
bool activatedUR;
bool activatedM;
bool activatedDL;
bool activatedDR;
bool debugMode;
bool autoMode;
int actSensUL;
int actSensUR;
int actSensM;
int actSensDL;
int actSensDR;
int thresSensUL;
int thresSensUR;
int thresSensM;
int thresSensDL;
int thresSensDR;
int lastInputUL;
int lastInputUR;
int lastInputM;
int lastInputDL;
int lastInputDR;
unsigned long timerUL;
unsigned long timerUR;
unsigned long timerM;
unsigned long timerDL;
unsigned long timerDR;
SoftwareSerial SUART(2, 3);

//These variables will only be used if extraMode is true. Can only be enabled through the mobile app.
//inputRounding = Rounds input forces to the nearest InputRounding value.
//maxThresSens = The highest value the arduino will set thresSens to for that panel in auto mode
//maxThresSens = The lowest value the arduino will set thresSens to for that panel in auto mode
int maxThresSensUL = 200;
int maxThresSensUR = 200;
int maxThresSensM = 200;
int maxThresSensDL = 200;
int maxThresSensDR = 200;
int minThresSensUL = 50;
int minThresSensUR = 50;
int minThresSensM = 50;
int minThresSensDL = 50;
int minThresSensDR = 50;
int maxActSensUL = 550;
int maxActSensUR = 550;
int maxActSensM = 550;
int maxActSensDL = 550;
int maxActSensDR = 550;
int minActSensUL = 300;
int minActSensUR = 300;
int minActSensM = 300;
int minActSensDL = 300;
int minActSensDR = 300;

//1 for UL, 2 for UR, 3 for M, 4 for DL, 5 for DR
void readjustSensitivity(int direction, int lastInput) {
  if(lastInput < 175) {
	  lastInput = 175;
  }
  else if(lastInput > 825){
	  lastInput = 775;
  }
  switch(direction){
    case 1:
      actSensUL = lastInput + 125;
      if(actSensUL > maxActSensUL) {
        actSensUL = maxActSensUL;
      }
      else if(actSensUL < minActSensUL) {
        actSensUL = minActSensUL;
      }
      thresSensUL = lastInput - 125;
      if(thresSensUL > maxThresSensUL) {
        thresSensUL = maxThresSensUL;
      }
      else if(thresSensUL < minThresSensUL) {
        thresSensUL = minThresSensUL;
      }
      timerUL = millis();
      break;
    case 2:
      actSensUR = lastInput + 125;
      if(actSensUR > maxActSensUR) {
        actSensUR = maxActSensUR;
      }
      else if(actSensUR < minActSensUR) {
        actSensUR = minActSensUR;
      }
      thresSensUR = lastInput - 125;
      if(thresSensUR > maxThresSensUR) {
        thresSensUR = maxThresSensUR;
      }
      else if(thresSensUR < minThresSensUR) {
        thresSensUR = minThresSensUR;
      }
      timerUR = millis();
      break;
    case 3:
      actSensM = lastInput + 125;
      if(actSensM > maxActSensM) {
        actSensM = maxActSensM;
      }
      else if(actSensM < minActSensM) {
        actSensM = minActSensM;
      }
      thresSensM = lastInput - 125;
      if(thresSensM > maxThresSensM) {
        thresSensM = maxThresSensM;
      }
      else if(thresSensM < minThresSensM) {
        thresSensM = minThresSensM;
      }
      timerM = millis();
      break;
    case 4:
      actSensDL = lastInput + 125;
      if(actSensDL > maxActSensDL) {
        actSensDL = maxActSensDL;
      }
      else if(actSensUL < minActSensUL) {
        actSensDL = minActSensDL;
      }
      thresSensDL = lastInput - 125;
      if(thresSensDL > maxThresSensDL) {
        thresSensDL = maxThresSensDL;
      }
      else if(thresSensDL < minThresSensDL) {
        thresSensDL = minThresSensDL;
      }
      timerDL = millis();
      break;
    case 5:
      actSensDR = lastInput + 125;
      if(actSensDR > maxActSensDR) {
        actSensDR = maxActSensDR;
      }
      else if(actSensDR < minActSensDR) {
        actSensDR = minActSensDR;
      }
      thresSensDR = lastInput - 125;
      if(thresSensDR > maxThresSensDR) {
        thresSensDR = maxThresSensDR;
      }
      else if(thresSensDR < minThresSensDR) {
        thresSensDR = minThresSensDR;
      }
      timerDR = millis();
      break;
    default:
      actSensUL = 450;
      thresSensUL = 100;
      actSensUR = 450;
      thresSensUR = 100;
      actSensM = 450;
      thresSensM = 100;
      actSensDL = 450;
      thresSensDL = 100;
      actSensDR = 450;
      thresSensDR = 100;
      timerUR = millis();
      timerUL = millis();
      timerM = millis();
      timerDL = millis();
      timerDR = millis();
  }
}

//Checks the upper left panel for stuck or undersensitivity. Automatically calls readjustSensitivity if a correction needs to be made.
class SensorCorrectionUL : public VariableTimedAction {
private:
  unsigned long run() {
    if(!autoMode) {
      this->stop();
    }
    int currInput = analogRead(fsrpinUL);
    if(currInput < lastInputUL && lastInputUL > actSensUL - 50 && currInput < actSensUL && !activatedUL) { //Check for input that was close to activating the sensor but did not.
      if(timerUL + 500 < millis()) {
        readjustSensitivity(1,lastInputUL - 150);
      }
    }
    else if(lastInputUL > thresSensUL && lastInputUL < thresSensUL + 70 && currInput > lastInputUL && activatedUL && abs(currInput - lastInputUL) < 35 ) { //Check for panels that are stuck.
      if(timerUL + 500 < millis()) {
        readjustSensitivity(1,lastInputUL + 150);
      }
    }
    lastInputUL = currInput;
    return 0;
  }
};

//Same function as SensorCorrectionUL except for the UR panel
class SensorCorrectionUR : public VariableTimedAction {
private:
  unsigned long run() {
    if(!autoMode) {
      this->stop();
    }
    int currInput = analogRead(fsrpinUR);
    if(currInput < lastInputUR && lastInputUR > actSensUR - 50 && currInput < actSensUR && !activatedUR) {
      if(timerUR + 500 < millis()) {
        readjustSensitivity(2,lastInputUR - 150);
      }
    }
    else if(lastInputUR > thresSensUR && lastInputUR < thresSensUR + 50 && currInput > lastInputUR && activatedUR && abs(currInput - lastInputUR) < 20 ) {
      if(timerUR + 500 < millis()) {
        readjustSensitivity(2,lastInputUR + 150);
      }
    }
    lastInputUR = currInput;
    return 0;
  }
};

//Same function as SensorCorrectionUL except for the M panel
class SensorCorrectionM : public VariableTimedAction {
private:
  unsigned long run() {
    if(!autoMode) {
      this->stop();
    }
    int currInput = analogRead(fsrpinM);
    if(currInput < lastInputM && lastInputM > actSensM - 50 && currInput < actSensM && !activatedM) {
      if(timerM + 500 < millis()) {
        readjustSensitivity(3,lastInputM - 150);
      }
    }
    else if(lastInputM > thresSensM && lastInputM < thresSensM + 50 && currInput > lastInputM && activatedM && abs(currInput - lastInputM) < 20 ) {
      if(timerM + 500 < millis()) {
        readjustSensitivity(3,lastInputM + 150);
      }
    }
    lastInputM = currInput;
    return 0;
  }
};

//Same function as SensorCorrectionUL except for the DL panel
class SensorCorrectionDL : public VariableTimedAction {
private:
  unsigned long run() {
    if(!autoMode) {
      this->stop();
    }
    int currInput = analogRead(fsrpinDL);
    if(currInput < lastInputDL && lastInputDL > actSensDL - 50 && currInput < actSensDL && !activatedDL) {
      if(timerDL + 500 < millis()) {
        readjustSensitivity(4,lastInputDL - 150);
      }
    }
    else if(lastInputDL > thresSensDL && lastInputDL < thresSensDL + 50 &&  currInput > lastInputDL && activatedDL && abs(currInput - lastInputDL) < 20 ) {
      if(timerDR + 500 < millis()) {
        readjustSensitivity(4,lastInputDL + 150);
      }
    }
    lastInputDL = currInput;
    return 0;
  }
};

//Same function as SensorCorrectionUL except for the DR panel
class SensorCorrectionDR : public VariableTimedAction {
private:
  unsigned long run() {
    if(!autoMode) {
      this->stop();
    }
    int currInput = analogRead(fsrpinDR);
    if(currInput < lastInputDR && lastInputDR > thresSensDR - 50 && currInput < actSensDR && !activatedDR) {
      if(timerDR + 500 < millis()) {
        readjustSensitivity(5,lastInputDR - 150);
      }
    }
    else if(lastInputDR > thresSensDR && lastInputDR < thresSensDR + 50 &&  currInput > lastInputDR && activatedDR && abs(currInput - lastInputDL) < 20 ) {
      if(timerDR + 500 < millis()) {
        readjustSensitivity(5,lastInputDR + 150);
      }
    }
    lastInputDR = currInput;
    return 0;
  }
};

class OutputDebug : public VariableTimedAction {
private: 
  unsigned long run() {
    if(!debugMode) {
      this->stop();
    }
    SUART.println((String)("UL,") + maxActSensUL + "," + actSensUL + "," + minActSensUL + "," + analogRead(fsrpinUL) + "," + maxThresSensUL + "," + thresSensUL + "," + minThresSensUL);
    SUART.println((String)("UR,") + maxActSensUR + "," + actSensUR + "," + minActSensUR + "," + analogRead(fsrpinUR) + "," + maxThresSensUR + "," + thresSensUR + "," + minThresSensUR);
    SUART.println((String)("M,") + maxActSensM + "," + actSensM + "," + minActSensM + "," + analogRead(fsrpinM) + "," + maxThresSensM + "," + thresSensM + "," + minThresSensM);
    SUART.println((String)("DL,") + maxActSensDL + "," + actSensDL + "," + minActSensDL + "," + analogRead(fsrpinDL) + "," + maxThresSensDL + "," + thresSensDL + "," + minThresSensDL);
    SUART.println((String)("DR,") + maxActSensDR + "," + actSensDR + "," + minActSensDR + "," + analogRead(fsrpinDR) + "," + maxThresSensDR + "," + thresSensDR + "," + minThresSensDR);
    Serial.println((String)("UL,") + maxActSensUL + "," + actSensUL + "," + minActSensUL + "," + analogRead(fsrpinUL) + "," + maxThresSensUL + "," + thresSensUL + "," + minThresSensUL);
    Serial.println((String)("UR,") + maxActSensUR + "," + actSensUR + "," + minActSensUR + "," + analogRead(fsrpinUR) + "," + maxThresSensUR + "," + thresSensUR + "," + minThresSensUR);
    Serial.println((String)("M,") + maxActSensM + "," + actSensM + "," + minActSensM + "," + analogRead(fsrpinM) + "," + maxThresSensM + "," + thresSensM + "," + minThresSensM);
    Serial.println((String)("DL,") + maxActSensDL + "," + actSensDL + "," + minActSensDL + "," + analogRead(fsrpinDL) + "," + maxThresSensDL + "," + thresSensDL + "," + minThresSensDL);
    Serial.println((String)("DR,") + maxActSensDR + "," + actSensDR + "," + minActSensDR + "," + analogRead(fsrpinDR) + "," + maxThresSensDR + "," + thresSensDR + "," + minThresSensDR);
    return 0;
  }
};

SensorCorrectionUL correctionUL;
SensorCorrectionUR correctionUR;
SensorCorrectionM correctionM;
SensorCorrectionDL correctionDL;
SensorCorrectionDR correctionDR;
OutputDebug outputDBG;

void setup() {
  Serial.begin(9600);
  SUART.begin(9600);
  pinMode(12, INPUT);
  pinMode(9, INPUT);
  if(digitalRead(12) == HIGH) {
    autoMode = true;
	  readjustSensitivity(0,-1);
	  correctionUL.start(100);
    correctionUR.start(100);
    correctionM.start(100);
    correctionDL.start(100);
    correctionDR.start(100);
  }
  else { //Edit these values for the non auto adjusting mode.
	  actSensUL = 450;
	  actSensUR = 450;
	  actSensM = 450;
	  actSensDL = 450;
	  actSensDR = 450;
	  thresSensUL = 100;
	  thresSensUR = 100;
	  thresSensM = 100;
	  thresSensDL = 100;
	  thresSensDR = 100;
  }
  if(digitalRead(9) == HIGH) { 
    debugMode = true;
    outputDBG.start(16);
  }
}

void loop() {
  if (analogRead(fsrpinUL) < thresSensUL) {
    activatedUL = false;
    Keyboard.release('t');
  } 
  if (analogRead(fsrpinUL)> actSensUL) {
    activatedUL = true;
    Keyboard.press('t');
  }
  if (analogRead(fsrpinUR) < thresSensUR) {
    activatedUR = false;
    Keyboard.release('u');
  } 
  if (analogRead(fsrpinUR) > actSensUR) {
    activatedUR = true;
    Keyboard.press('u');
  } 
  if (analogRead(fsrpinM) < thresSensM) {
    activatedM = false;
    Keyboard.release('h');
  } 
  if (analogRead(fsrpinM) > actSensM) {
    activatedM = true;
    Keyboard.press('h');
  }
  if (analogRead(fsrpinDL) < thresSensDL) {
    activatedDL = false;
    Keyboard.release('b');
  } 
  if (analogRead(fsrpinDL) > actSensDL) {
    activatedDL = true;
    Keyboard.press('b');
  }
  if (analogRead(fsrpinDR) < thresSensDR) {
    activatedDR = false;
    Keyboard.release('m');
  }
  if (analogRead(fsrpinDR) > actSensDR) {
    activatedDR = true;
    Keyboard.press('m');
  }
  VariableTimedAction::updateActions();
}

//Bluetooth functionality. Allows for more options, manual control, and debugging of the program through a mobile app.
void serialEvent() {
  if(Serial.available() > 0)  
  {
    char* stored = strdup(Serial.readString().c_str());
    int func = atoi(strtok(stored,","));
    int val1 = atoi(strtok(NULL,","));
    int val2 = atoi(strtok(NULL,","));
    int val3 = atoi(strtok(NULL,","));
    int val4 = atoi(strtok(NULL,","));
    switch(func){ //0 for reset, 1-5 for manual panel settings adjustment, 10/11 for enabling/disabling auto adjustment feature
      case 0: 
        readjustSensitivity(0,-1);
        break;
	    case 1:
        minActSensUL = val1;
        maxActSensUL = val2;
		    minThresSensUL = val3;
        maxThresSensUL = val4;
        timerUL = millis();
        break;
      case 2:
        minActSensUR = val1;
        maxActSensUR = val2;
		    minThresSensUR = val3;
        maxThresSensUR = val4;
        timerUR = millis();
        break;
      case 3:
        minActSensM = val1;
        maxActSensM = val2;
		    minThresSensM = val3;
        maxThresSensM = val4;
        timerM = millis();
        break;
      case 4:
        minActSensDL = val1;
        maxActSensDL = val2;
		    minThresSensDL = val3;
        maxThresSensDL = val4;
        timerDL = millis();
        break;
      case 5:
        minActSensDR = val1;
        maxActSensDR = val2;
		    minThresSensDR = val3;
        maxThresSensDR = val4;
        timerDR = millis();
        break;
      case 10:
        actSensUL = val1;
        thresSensUL = val2;
        break;
      case 20:
        actSensUR = val1;
        thresSensUR = val2;
        break;
      case 30:
        actSensM = val1;
        thresSensM = val2;
        break;
      case 40:
        actSensDL = val1;
        thresSensDL = val2;
        break;
      case 50:
        actSensDR = val1;
        thresSensDR = val2;
        break;
      case 100:
        if(autoMode) {
          autoMode = false;
          correctionUL.stop();
          correctionUR.stop();
          correctionM.stop();
          correctionDL.stop();
          correctionDR.stop();
        }
        break;
      case 110:
        if(!autoMode) {
          autoMode = true;
          correctionUL.start(100);
          correctionUR.start(100);
          correctionM.start(100);
          correctionDL.start(100);
          correctionDR.start(100);
        }
        break;
      case 200:
        if(debugMode){
          debugMode = false;
          outputDBG.stop();
        }
        break;
      case 210:
        if(!debugMode){
          debugMode = true;
          outputDBG.start(16);
        }
        break;   
    }   
  }  
}
