# 5-panel FSR Arduino controller V1.3
**Created by Andy Banh**

This is an Arduino project created to handle 4 and 5-panel FSR-sensor dance pads used with Stepmania and its derivatives.

**PLEASE DO NOT** try and run this with traditional pad sensors, contacting plates, or load cells as they are not supported.

This program features an auto-sensitivity adjustment feature which can be enabled by bridging pin 12 to ground. A debugging feature is also available by bridging pin 9 to ground.

These options are also available through FSRControl App (Does not exist yet) and even more options will be available through the mobile app that will not be available through manually plugging wires through the Arduino alone.

If you need assistance with how to wire up your FSRs to your Arduino, please check this guide:
https://learn.adafruit.com/force-sensitive-resistor-fsr/using-an-fsr

For reading data from the Serial output (Debug Mode), please check this guide:
https://create.arduino.cc/projecthub/HackingSTEM/stream-data-from-arduino-into-excel-f1bede

For help with adding bluetooth functionality to your Arduino, please check these links: 
https://create.arduino.cc/projecthub/mayooghgirish/arduino-bluetooth-basic-tutorial-d8b737
https://www.amazon.com/HiLetgo-Wireless-Bluetooth-Transceiver-Arduino/dp/B071YJG8DR


Recommendations
---------------

- A default value of 450 is assigned to actuation and 100 for deactuation for the non-auto adjusting setting.
You can change these values to match your implemented resistor and preferred sensitivity under the setup() function through the variables maxsens and thresSens.

- A 10k resistor is highly recommended.

- For 10k ohm resistors, I do not recommend using values under 50 for thresSens as panel weight/shape and spacer types can introduce unintended forces to your sensor and values over 900 as readings above that number are unreliable. If you are using a higher or lower ohm resistor then these values can be changed according to your resistor.

# Changelog

Version 1.0 - Initial build. Support for 5-panel FSRs dance pads implemented.

Version 1.1 - Experimental auto sensitivty adjustment feature implemented.

Version 1.2 - Rewrite to use VariableTimedAction library to check force differences in intervals rather than averages over larger duration of time.

Version 1.21 - Removal of adjustment aggression due to each option not making a large difference in adjustments. Support for bluetooth added.

Version 1.3 - Fixed auto sensitivity adjustment to work without issues and VariableTimedAction to properly function.
